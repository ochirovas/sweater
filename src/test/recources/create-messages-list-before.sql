delete from message;

insert into message(id, text, tag, user_id) values
(1, 'first', 'my-tag', 1),
(2, 'second', 'more', 2),
(3, 'third', 'my-tag', 3),
(4, 'fourth', 'another', 2);

alter sequence hibernate_sequence restart with 10;