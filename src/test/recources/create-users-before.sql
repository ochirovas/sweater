delete from user_role;
delete from message;
delete from usr;

insert into usr(id, active, password, username) values
(1, true, 'qwe', 'admin'),
(2, true, '123', 'ochirov_alexander'),
(3, true, '123', 'sania');

create extension if not exists pgcrypto;
update usr set password = crypt(password, gen_salt('bf', 8));

insert into user_role(user_id, roles) values
(1, 'USER'), (1, 'ADMIN'),
(2, 'USER'),
(3, 'USER');
