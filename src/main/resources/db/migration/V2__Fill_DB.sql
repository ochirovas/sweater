insert into usr (id, username, password, active) values
(1, 'admin', 'qwe', true),
(2, 'ochirov_alexander', '123', true),
(3, 'sania', '123', true);

insert into user_role (user_id, roles) values
(1, 'USER'), (1, 'ADMIN'),
(2, 'USER'),
(3, 'USER');

create extension if not exists pgcrypto;

update usr set password = crypt(password, gen_salt('bf', 8));

insert into message(id, text, tag, user_id) values
(1, 'space', 'cat', 1),
(2, 'Bojack', 'Horseman', 2),
(3, 'some', 'girl', 3),
(4, 'mister', 'X', 2)


